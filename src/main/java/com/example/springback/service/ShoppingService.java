package com.example.springback.service;

import com.example.springback.model.Item;
import com.example.springback.model.ShoppingCart;
import com.example.springback.model.User;
import com.example.springback.repository.ItemRepository;
import com.example.springback.repository.ShoppingCartRepository;
import com.example.springback.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class ShoppingService {

    private UserRepository userRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ItemRepository itemRepository;

    @Autowired
    public ShoppingService(UserRepository userRepository, ShoppingCartRepository shoppingCartRepository, ItemRepository itemRepository) {
        this.userRepository = userRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.itemRepository = itemRepository;
    }

    public Item addItemToCart(Long id) {
        ShoppingCart shoppingCart = getShoppingCart();
        Item item = itemRepository.findById(id).get(); // needs to be checked if there is a value
        shoppingCart.getItems().add(item);
        shoppingCartRepository.save(shoppingCart);
        return item;
    }

    public ShoppingCart getShoppingCart() {
        String name =
                SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findByName(name);
        ShoppingCart shoppingCart = shoppingCartRepository.findByUser_id(user.getId());
        return shoppingCart;
    }

}
