package com.example.springback;

import com.example.springback.model.Item;
import com.example.springback.model.ShoppingCart;
import com.example.springback.model.User;
import com.example.springback.repository.ItemRepository;
import com.example.springback.repository.ShoppingCartRepository;
import com.example.springback.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class SpringBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBackApplication.class, args);
	}

	@Bean
	CommandLineRunner init(
			UserRepository userRepository,
			ItemRepository itemRepository,
			ShoppingCartRepository shoppingCartRepository) {
		return args -> {
			Stream.of("John","a", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
				User user = new User(name, name.toLowerCase() + "@domain.com");
				user = userRepository.save(user);
				ShoppingCart shoppingCart = new ShoppingCart();
				shoppingCart.setUser(user);
				shoppingCartRepository.save(shoppingCart);

			});

			Stream.of("Bike","Car","Bread","Bagels").forEach( itemName ->{
				Item item = new Item(itemName);
				itemRepository.save(item);
			});

			userRepository.findAll().forEach(System.out::println);
			shoppingCartRepository.findAll().forEach(System.out::println);
			itemRepository.findAll().forEach(System.out::println);
		};
	}
}
