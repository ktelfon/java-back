package com.example.springback.controller;

import com.example.springback.model.Item;
import com.example.springback.model.ShoppingCart;
import com.example.springback.service.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class ShoppingController {

    private ShoppingService shoppingService;

    @Autowired
    public ShoppingController(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @GetMapping("add/{id}")
    public Item addItemToCart(@PathVariable("id") Long id){
        return shoppingService.addItemToCart(id);
    }

    @GetMapping("getCart")
    public ShoppingCart getCart(){
        return shoppingService.getShoppingCart();
    }
}
