package com.example.springback.controller;

import com.example.springback.model.User;
import com.example.springback.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/user/all")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/user")
    public void save(@RequestBody User user){
        userRepository.save(user);
    }

    @GetMapping("/user/{id}")
    public User getById(@PathVariable("id") Long id){
        return userRepository.findById(id).orElse(new User());
    }

    @GetMapping("/user/current")
    public User getCurrentUser(){
        return  userRepository.findByName(SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName());
    }
}
